package tankgame;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Scanner;

public class TankGame extends JFrame {

    //定义MyPanel
    MyPanel mp = null;
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        TankGame tankGame = new TankGame();
    }

    public TankGame() {
        System.out.println("请输入选择 1： 新游戏 2： 继续上局");
        String key =in.next();
        mp = new MyPanel(key);
        //将mp放入到Thread，并启动
        Thread thread = new Thread(mp);
        thread.start();

        this.add(mp);      // 将一个 MyPanel 对象 mp 添加到当前的 JFrame 中，以便在窗口中显示 MyPanel 内部绘制的内容。
        this.setSize(1300, 750);  //设置游戏窗口的大小为宽度1000像素和高度750像素
        this.addKeyListener(mp); //让JFrame 监听mp的键盘事件
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //当用户关闭窗口时，应用程序将终止运行。
        this.setVisible(true);  //将游戏窗口设置为可见

        //在JFrame中增加相应关闭窗口的处理
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Recorder.keepRecord();
                System.exit(0);
            }
        });

    }
}
